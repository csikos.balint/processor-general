package hu.fnf.devel.processor;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.ByteArrayInputStream;
import java.io.OutputStream;

import static org.junit.Assert.*;

/**
 * Created by johnnym on 5/28/17.
 */
@RunWith(SpringRunner.class)
public class EntrypointTest {
    private String req;

    @Before
    public void setUp() {
        req = "{\n" +
                "  \"resource\": \"/myFunctionBalint\",\n" +
                "  \"path\": \"/myFunctionBalint\",\n" +
                "  \"httpMethod\": \"POST\",\n" +
                "  \"headers\": null,\n" +
                "  \"queryStringParameters\": {\n" +
                "    \"request\": \"ooo\"\n" +
                "  },\n" +
                "  \"pathParameters\": null,\n" +
                "  \"stageVariables\": null,\n" +
                "  \"requestContext\": {\n" +
                "    \"path\": \"/myFunctionBalint\",\n" +
                "    \"accountId\": \"101519982611\",\n" +
                "    \"resourceId\": \"2fpeud\",\n" +
                "    \"stage\": \"test-invoke-stage\",\n" +
                "    \"requestId\": \"test-invoke-request\",\n" +
                "    \"identity\": {\n" +
                "      \"cognitoIdentityPoolId\": null,\n" +
                "      \"accountId\": \"101519982611\",\n" +
                "      \"cognitoIdentityId\": null,\n" +
                "      \"caller\": \"AIDAI433BI5BXTFIGEPR4\",\n" +
                "      \"apiKey\": \"test-invoke-api-key\",\n" +
                "      \"sourceIp\": \"test-invoke-source-ip\",\n" +
                "      \"accessKey\": \"ASIAI2Z3SRJ2KFW2UBNQ\",\n" +
                "      \"cognitoAuthenticationType\": null,\n" +
                "      \"cognitoAuthenticationProvider\": null,\n" +
                "      \"userArn\": \"arn:aws:iam::101519982611:user/balint.csikos@ge.com\",\n" +
                "      \"userAgent\": \"Apache-HttpClient/4.5.x(Java/1.8.0_112)\",\n" +
                "      \"user\": \"AIDAI433BI5BXTFIGEPR4\"\n" +
                "    },\n" +
                "    \"resourcePath\": \"/myFunctionBalint\",\n" +
                "    \"httpMethod\": \"POST\",\n" +
                "    \"apiId\": \"14zqejo29j\"\n" +
                "  },\n" +
                "  \"body\": \"{\\n\\\"a\\\":\\\"A\\\",\\n\\\"b\\\":\\\"B\\\"\\n}\",\n" +
                "  \"isBase64Encoded\": false\n" +
                "}";
    }

    @Test
    public void handleRequest() throws Exception {
        Entrypoint entrypoint = new Entrypoint();
        entrypoint.handleRequest(new ByteArrayInputStream(req.getBytes()),System.out, null);
    }

}
