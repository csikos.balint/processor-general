package hu.fnf.devel.processor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
@SpringBootApplication
public class Application {
    private static ConfigurableApplicationContext context = SpringApplication.run(Application.class);

    public static void main(String[] args) {
        //no need to start
    }

    public static ConfigurableApplicationContext getContext() {
        return context;
    }
}
