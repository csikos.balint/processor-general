package hu.fnf.devel.processor.resource.model;

import com.fasterxml.jackson.databind.JsonNode;

public class Message {
    public static final String MESSAGE_NODE = "body";
    String msg;

    public Message(JsonNode node) {
        try {
            msg = node.get(MESSAGE_NODE).asText();
        } catch (NullPointerException e) {
            msg = "";
        }
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
