package hu.fnf.devel.processor.resource;

import com.fasterxml.jackson.databind.JsonNode;
import hu.fnf.devel.processor.resource.model.Message;
import org.springframework.stereotype.Component;

@Component
public class Processor {
    public Message process(JsonNode node) {
        return new Message(node);
    }
}
