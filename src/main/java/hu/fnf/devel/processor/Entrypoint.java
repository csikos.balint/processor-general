package hu.fnf.devel.processor;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import hu.fnf.devel.processor.resource.Processor;
import hu.fnf.devel.processor.resource.model.Message;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.*;
import java.net.HttpURLConnection;
import java.util.Map;
import java.util.Scanner;

public class Entrypoint implements RequestStreamHandler {
    /**
     * https://docs.aws.amazon.com/lambda/latest/dg/java-programming-model-req-resp.html
     */
    public void handleRequest(InputStream inputStream, OutputStream outputStream, Context context) throws IOException {

//        LambdaLogger logger = context.getLogger();
//        logger.log("Loading Java Lambda handler of ProxyWithStream");


        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        ResponseClass response = new ResponseClass();
        response.setStatusCode(HttpURLConnection.HTTP_OK);

        StringBuilder request = new StringBuilder();
        Scanner scanner = new Scanner(inputStream);

        while (scanner.hasNext() ) {
            request.append(scanner.next());
        }
        JsonNode node = new ObjectMapper().readTree(request.toString());
        response.setBody(node.toString());

        Processor processor = ((Processor)Application.context.getBean("processor"));
        Message message = processor.process(node);

        OutputStreamWriter writer = new OutputStreamWriter(outputStream, "UTF-8");
        writer.write(new ObjectMapper().writeValueAsString(message));
        writer.close();
    }

    public static void main(String[] args) {
        //none
    }

    private class ResponseClass {
        private int statusCode;
        private Map<String, String> headers;
        private String body;

        public int getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(int statusCode) {
            this.statusCode = statusCode;
        }

        public Map<String, String> getHeaders() {
            return headers;
        }

        public void setHeaders(Map<String, String> headers) {
            this.headers = headers;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }
    }
}
